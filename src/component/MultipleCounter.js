import CounterSizeGenerator from "./CounterSizeGenerator"
import CounterGroup from "./CounterGroup"
import CounterGroupSum from "./CounterGroupSum"

const MultipleCounter = () => {

    return (
        <div>
            <CounterSizeGenerator></CounterSizeGenerator>
            <CounterGroupSum></CounterGroupSum>
            <CounterGroup></CounterGroup>
        </div>
    )
}

export default MultipleCounter