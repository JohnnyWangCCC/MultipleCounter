import Counter from './Counter';
import { useSelector } from "react-redux"

const CounterGroup = () => {
    const countList = useSelector(state => state.counter.countList)

    return (
        <div>
            {countList.map((counter, index) => {
                return <Counter key={index} value={counter} index={index}></Counter>
                })}
        </div>
    );
}

export default CounterGroup